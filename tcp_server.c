#include <sys/poll.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <poll.h>

#include "util.h"


#define LISTEN_PORT ((uint16_t) 32546)
#define BUFFER_LEN ((size_t) 512)
#define POLLFD_LEN ((size_t) 128)


int main() {

    // Create socket file descriptor
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    // Initialize server address.
    struct sockaddr_in listen_addr = {0};
    listen_addr.sin_family = AF_INET;
    listen_addr.sin_port = htons(LISTEN_PORT);
    listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    printf("[TCP] Listening on port %hu\n", LISTEN_PORT);

    // Bind the socket in order to specify a port.
    int res = bind(fd, (struct sockaddr *) &listen_addr, sizeof(listen_addr));
    if (res < 0) {
        close(fd);
        printf("[TCP] Error while binding: %s\n", strerror(errno));
        return 1;
    }

    if (listen(fd, 10) < 0) {
        close(fd);
        printf("[TCP] Error while making socket passive: %s\n", strerror(errno));
        return 1;
    }

    char *buffer = malloc(BUFFER_LEN);
    struct pollfd *fds = calloc(POLLFD_LEN, sizeof(struct pollfd));
    int fds_count = 0;

    struct sockaddr client_addr;
    socklen_t client_addr_len;

    // Le premier pollfd correspond à notre socket d'écoute.
    fds[0].fd = fd;
    fds[0].events = POLLIN;

    while (1) {

        if (poll(fds, fds_count + 1, -1) < 0) {
            printf("[TCP] Error while polling: %s\n", strerror(errno));
            return 1;
        }

        if (fds[0].revents & POLLIN) {

            // Should not block.
            client_addr_len = sizeof(struct sockaddr);
            int client_fd = accept(fds[0].fd, &client_addr, &client_addr_len);
            if (client_fd < 0) {
                printf("[TCP] Error while accepting client: %s\n", strerror(errno));
                return 1;
            }

            client_addr_len = sizeof(struct sockaddr);
            getpeername(client_fd, &client_addr, &client_addr_len); // Note: might error
            printf("[TCP] Accept client: ");
            print_sockaddr(&client_addr);
            printf("\n");

            fds_count++;
            fds[fds_count].fd = client_fd;
            fds[fds_count].events = POLLIN;

        }

        for (int i = 1; i <= fds_count; i++) {
            if (fds[i].revents & POLLIN) {

                int client_fd = fds[i].fd;

                client_addr_len = sizeof(struct sockaddr);
                getpeername(client_fd, &client_addr, &client_addr_len); // Note: might error
                printf("[TCP] <");
                print_sockaddr(&client_addr);
                printf("> ");

                ssize_t recv_len;
                if ((recv_len = recv(client_fd, buffer, BUFFER_LEN, 0)) < 0) {
                    printf("Error while receiving: %s\n", strerror(errno));
                    continue;
                }

                if (recv_len == 0) {
                    close(client_fd);
                    fds[i].fd = 0;
                    printf("Disconnected.\n");
                    continue;
                }

                printf("Received stream: ");
                print_buffer(buffer, recv_len);
                printf("\n");

            }
        }

    }

}
