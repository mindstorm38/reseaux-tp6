#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char **argv) {

    if (argc != 3) {
        printf("usage: %s <addr> <port>\n", argv[0]);
        return 1;
    }

    // Create socket file descriptor
    int fd = socket(AF_INET, SOCK_STREAM, 0);

    // Initialize server address.
    struct sockaddr_in send_addr = {0};
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(atoi(argv[2]));
    inet_aton(argv[1], &send_addr.sin_addr);

    if (connect(fd, (struct sockaddr *) &send_addr, sizeof(send_addr)) < 0) {
        close(fd);
        printf("[TCP] Error while connection: %s\n", strerror(errno));
        return 1;
    }

    char *line = NULL;
    size_t line_len = 0;
    ssize_t len;

    while (1) {

        printf("> ");
        fflush(stdout);

        len = getline(&line, &line_len, stdin);
        len = send(fd, line, len, 0);

        if (len < 0) {
            close(fd);
            printf("[TCP] Error while sending: %s\n", strerror(errno));
            return 1;
        }

        printf("[TCP] Sent %d bytes to %s:%s\n", (int) len, argv[1], argv[2]);

    }

    if (line) {
        free(line);
    }

}
