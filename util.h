#include <netinet/in.h>
#include <stddef.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>


static void print_buffer(const char *buf, size_t len) {
    while (len > 0) {
        switch (*buf) {
            case '\n':
                printf("\\n");
                break;
            case '\r':
                printf("\\r");
                break;
            default:
                printf("%c", *buf);
        }
        len--;
        buf++;
    }
}


static void print_sockaddr(const struct sockaddr *addr) {
    char buf[128];
    if (addr->sa_family == AF_INET) {
        const struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;
        inet_ntop(AF_INET, &addr_in->sin_addr, buf, sizeof(struct sockaddr_in));
        printf("%s:%d", buf, htons(addr_in->sin_port));
    } else if (addr->sa_family == AF_INET6) {
        const struct sockaddr_in6 *addr_in = (struct sockaddr_in6 *) addr;
        inet_ntop(AF_INET6, &addr_in->sin6_addr, buf, sizeof(struct sockaddr_in6));
        printf("[%s]:%d", buf, htons(addr_in->sin6_port));
    }
}
