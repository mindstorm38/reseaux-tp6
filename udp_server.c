#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>

#include "util.h"


#define LISTEN_PORT ((uint16_t) 32546)
#define BUFFER_LEN ((size_t) 512)


int main() {

    // Create socket file descriptor
    int fd = socket(AF_INET, SOCK_DGRAM, 0);

    // Initialize server address.
    struct sockaddr_in listen_addr = {0};
    listen_addr.sin_family = AF_INET;
    listen_addr.sin_port = htons(LISTEN_PORT);
    listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    printf("[UDP] Listening on port %hu\n", LISTEN_PORT);

    // Bind the socket in order to specify a port.
    int res = bind(fd, (struct sockaddr *) &listen_addr, sizeof(listen_addr));
    if (res != 0) {
        close(fd);
        printf("[UDP] Error while binding: %s\n", strerror(errno));
        return 1;
    }

    struct sockaddr recv_addr;
    socklen_t recv_addr_len;
    ssize_t recv_len;
    char *buffer = malloc(BUFFER_LEN);

    while (1) {

        if ((recv_len = recvfrom(fd, buffer, BUFFER_LEN, 0, &recv_addr, &recv_addr_len)) < 0) {
            close(fd);
            printf("[UDP] Error while receiving: %s\n", strerror(errno));
            return 1;
        }

        printf("[UDP] Received datagram: ");
        print_buffer(buffer, recv_len);
        printf("\n");

    }

}
