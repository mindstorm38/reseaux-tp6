#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char **argv) {

    if (argc != 4) {
        printf("usage: %s <addr> <port> <msg>\n", argv[0]);
        return 1;
    }

    // Create socket file descriptor
    int fd = socket(AF_INET, SOCK_DGRAM, 0);

    // Initialize server address.
    struct sockaddr_in send_addr = {0};
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(atoi(argv[2]));
    inet_aton(argv[1], &send_addr.sin_addr);

    ssize_t res = sendto(fd, argv[3], strlen(argv[3]), 0, (struct sockaddr *) &send_addr, sizeof(send_addr));
    if (res < 0) {
        close(fd);
        printf("[UDP] Error while sending: %s\n", strerror(errno));
        return 1;
    }

    close(fd);
    printf("[UDP] Sent %d bytes to %s:%s\n", (int) res, argv[1], argv[2]);

}
